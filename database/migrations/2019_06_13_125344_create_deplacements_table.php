<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deplacements', function (Blueprint $table) {
         $table->increments('id');
           $table->string('nom');
            $table->string('prenom');
            $table->string('email');
            $table->date('datedepart');
            $table->date('dateretour');   
            $table->time('heuredepart');  
            $table->time('heureretour');
            $table->integer('ville_id')->unsigned();
            $table->foreign('ville_id')->references('id')->on('villes');
            $table->integer('moyen_id')->unsigned();
            $table->foreign('moyen_id')->references('id')->on('moyens');
            $table->timestamps();
          
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deplacements');
    }
}
