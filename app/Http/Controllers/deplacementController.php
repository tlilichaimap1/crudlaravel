<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\deplacement;
class deplacementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $deplacements = deplacement::all();

        return view('deplacements.index', compact('deplacements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('deplacements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'email'=>'required'
        ]);

        $deplacement = new deplacement([
            'nom' => $request->get('nom'),
            'prenom' => $request->get('prenom'),
            'email' => $request->get('email'),
            'datedepart' => $request->get('datedepart'),
            'dateretour' => $request->get('dateretour'),
            'heuredepart' => $request->get('heuredepart'),
            'heureretour' => $request->get('heureretour')
          
        ]);
        $deplacement->save();
        return redirect('/deplacements')->with('success', 'deplacement saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $deplacement = deplacement::find($id);
        return view('deplacements.edit', compact('deplacement'));     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'email'=>'required'
        ]);

        $deplacement = deplacement::find($id);
        $deplacement->nom =  $request->get('nom');
        $deplacement->prenom = $request->get('prenom');
        $deplacement->email = $request->get('email');
        $deplacement->datedepart = $request->get('datedepart');
        $deplacement->dateretour = $request->get('dateretour');
        $deplacement->heuredepart = $request->get('heuredepart');
        $deplacement->heureretour = $request->get('heureretour');
        $deplacement->save();

        return redirect('/deplacements')->with('success', 'deplacement updated!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deplacement = deplacement::find($id);
        $deplacement->delete();

        return redirect('/deplacements')->with('success', 'deplacement deleted!');
    }
}
