<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deplacement extends Model
{
   protected $fillable = [
        'nom',
        'prenom',
        'email',
        'datedepart',
        'dateretour',
       'heuredepart',
       'heureretour'
    ];
public $timestamps = false;

}
