@extends('base')

@section('main')
   <div>
    <a style="margin: 19px;" href="{{ route('deplacements.create')}}" class="btn btn-primary">New deplacement</a>
    </div>  
<div class="row">
<div class="col-sm-12">
    <h2 class="display-3">deplacements</h2>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Nom</td>
          <td>prenom</td>
          <td>email</td>
          <td>datedepart</td>
          <td>dateretour</td>
          
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($deplacements as $deplacement)
        <tr>
            <td>{{$deplacement->nom}} <td>{{$deplacement->prenom}}</td>
            <td>{{$deplacement->email}}</td>
            <td>{{$deplacement->datedepart}}</td>
            <td>{{$deplacement->dateretour}}</td>
           
            <td>
                <a href="{{ route('deplacements.edit',$deplacement->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('deplacements.destroy', $deplacement->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
@endsection
