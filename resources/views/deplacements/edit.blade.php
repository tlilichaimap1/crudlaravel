@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a deplacement</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('deplacements.update', $deplacement->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="nom">nom:</label>
                <input type="text" class="form-control" name="nom" value={{ $deplacement->nom }} />
            </div>

            <div class="form-group">
                <label for="prenom">prenom:</label>
                <input type="text" class="form-control" name="prenom" value={{ $deplacement->prenom }} />
            </div>

            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value={{ $deplacement->email }} />
            </div>
            <div class="form-group">
                <label for="datedepart">date depart:</label>
                <input type="date" class="form-control" name="datedepart" value={{ $deplacement->datedepart }} />
            </div>
            <div class="form-group">
                <label for="dateretour">date retour:</label>
                <input type="date" class="form-control" name="dateretour" value={{ $deplacement->dateretour }} />
            </div>
            <div class="form-group">
                <label for="heuredepart">heure depart:</label>
                <input type="time" class="form-control" name="heuredepart" value={{ $deplacement->heuredepart }} />
            </div>
 <div class="form-group">
                <label for="heureretour">heure retour:</label>
                <input type="time" class="form-control" name="heureretour" value={{ $deplacement->heureretour }} />
            </div>

            <button type="submit" class="btn btn-primary">Modifier</button>
        </form>
    </div>
</div>
@endsection
