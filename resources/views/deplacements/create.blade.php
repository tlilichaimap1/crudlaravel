@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h4 class="display-3">ajouter deplacement</h4>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('deplacements.store') }}">
          @csrf
          <div class="form-group">    
              <label for="nom">Nom</label>
              <input type="text" class="form-control" name="nom"/>
          </div>

          <div class="form-group">
              <label for="prenom">prenom</label>
              <input type="text" class="form-control" name="prenom"/>
          </div>

          <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control" name="email"/>
          </div>
          <div class="form-group">
              <label for="datedepart">date depart</label>
              <input type="date" class="form-control" name="datedepart"/>
          </div>
          <div class="form-group">
              <label for="dateretour">date retour:</label>
              <input type="date" class="form-control" name="dateretour"/>
          </div>
          <div class="form-group">
              <label for="heuredepart">heure depart:</label>
              <input type="time" class="form-control" name="heuredepart"/>
 <div class="form-group">
              <label for="heureretour">heure retour:</label>
              <input type="time" class="form-control" name="heureretour"/>
          </div>                         
          <button type="submit" class="btn btn-primary-outline">ajouter </button>
      </form>
  </div>
</div>
</div>
@endsection
